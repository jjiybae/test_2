# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


SLACK_TOKEN = 'xoxb-691564988023-691870077670-WdN3tjlipoEWX23qoPWIvqaV'
SLACK_SIGNING_SECRET ='dc9effe66a7666630eb0294481c48736'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    url = "https://music.bugs.co.kr"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    message =[]
    if not "music" in text:
        return "`@<봇이름> music` 과 같이 멘션해주세요."


    # 여기에 함수를 구현해봅시다.
    if 'music' in text :
        for i ,title in enumerate(soup.find_all("p", class_= "title")):
            if len(message) >= 10 :
                break
            message.append(title.get_text(i))

    return message


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
    )
